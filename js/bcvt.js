////////////////////////////////////////////////////
// Blender Cloud Video Toolbox for VideoJS
// DESC: Written to provide extra functionality
// to the VideoJS library
//
// Ver. 1.1a
////////////////////////////////////////////////////

var BCloud_VT = (function () {
  "use strict";

  var priv = function () {
    // Set all our variables and use these only when needed
  };

  return {
    storageIsSet: function (playerID) {

    },
    setTime: function (playerID) {

      if(localStorage) {
	var player = videojs(playerID);
	var videoSrc = player.currentSrc();
	var videoSrcFile = videoSrc.split("/").pop();
	var PlayheadSet = !!localStorage.getItem(videoSrcFile + "_playHead"); // Set Boolean check for localSotorage key

        if(PlayheadSet) {
          $("#resumeButton").show();
        } else {
          $("#resumeButton").hide();
        }

        // Test setting local storage
        localStorage.setItem(videoSrcFile + "_playHead", Math.round(parseInt(player.currentTime())));

        console.log("Local storage is active");
      } else {
        // Change this to display an error if the person does not have localStorage.
        console.log("Your browser does not support HTML5 localStorage");
      }
    },
    getTime: function (playerID) {

      var player = videojs(playerID);
      var videoSrc = player.currentSrc();
      var videoSrcFile = videoSrc.split("/").pop();

      var PlayheadSet = !!localStorage.getItem(videoSrcFile + "_playHead"); // Set Boolean check for localSotorage key

      // Earlier version of VideoJS uses the title attribute. Newer releases use the vjs-paused class.
      var videoState = $(playerID).attr("class");

      switch(videoState) {
        case 'vjs-paused':
          localStorage.setItem(videoSrcFile + "_playHead", Math.round(parseInt(player.currentTime())));

          break;

        case 'vjs-playing':
          player.play();

          break;

        case 'Resume':
          player.currentTime(parseInt(localStorage.getItem(videoSrcFile + "_playHead") - 5));
          player.play();

          break;

        default:
      }
    }
  };
})();
