<!DOCTYPE html>
<html>
	<head>
		<title>VideoJS playback resume</title>
		<script
			src="https://code.jquery.com/jquery-2.2.4.min.js"
  			integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  			crossorigin="anonymous"></script>
  			<script src="js/bcvt.js"></script>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
			rel="stylesheet"
			integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
			crossorigin="anonymous">
		<link href="http://vjs.zencdn.net/6.4.0/video-js.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
		integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<style>
			body>button {
				margin-top: 10px;
			}
			.pr_10 {
				padding-right: 10px;
			}
			.ml_10 {
				margin-left: 10px;
			}
		</style>
	</head>

	<body>

		<video
    		id="player"
		    class="video-js"
    		controls
    		preload="auto"
	    	poster="//vjs.zencdn.net/v/oceans.png"
    		data-setup='{}'>
	  		<source src="//vjs.zencdn.net/v/oceans.mp4" type="video/mp4"></source>
  			<source src="//vjs.zencdn.net/v/oceans.webm" type="video/webm"></source>
 				<source src="//vjs.zencdn.net/v/oceans.ogv" type="video/ogg"></source>
	  		<p class="vjs-no-js">
    			To view this video please enable JavaScript, and consider upgrading to a
			    web browser that
		    	<a href="http://videojs.com/html5-video-support/" target="_blank">
	    			supports HTML5 video
    			</a>
  			</p>
		</video>
		<button class="ml_10 btn btn-lg btn-success" title="Resume" id="resumeButton">
			<i class="fa fa-play-circle-o pr_10"></i>Resume Playback
		</button>
		<script src="http://vjs.zencdn.net/6.4.0/video.js"></script>
		<script src="./js/bcvt.js"></script>

	</body>


	<script>
		(function(){
			$("#player").on("click", function(){
				BCloud_VT.setTime("player");
			});
		})();
	</script>
</html>
